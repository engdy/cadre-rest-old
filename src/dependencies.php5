<?php

$container = $app->getContainer();

$container['db'] = function($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('cadre_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/cadre.log");
    $logger->pushHandler($file_handler);
    return $logger;
};
