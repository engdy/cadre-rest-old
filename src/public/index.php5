<?php

require __DIR__ . '/../vendor/autoload.php';

// Instantiate the app
$settings = require __DIR__ . '/../settings.php5';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../dependencies.php5';

// Register middleware
require __DIR__ . '/../middleware.php5';

// Register routes
require __DIR__ . '/../routes.php5';

// Run app
$app->run();
