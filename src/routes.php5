<?php

$app->get('/hello', function($request, $response, $args) {
    $response->getBody()->write("Heya!");
    foreach ($this->db->query("select name from user") as $row) {
        $response->getBody()->write($row['name']);
    }
    return $response;
});

$app->post('/websess', function($request, $response, $args) {
    $body = $request->getParsedBody();
    $user = $body['user'];
    $pass = $body['pass'];
    $stmt = $this->db->prepare("SELECT user_id, name, email, tz, locale, image_url, admin, active " .
    	    		       "FROM user WHERE nick = ? AND password = password(?)");
    $stmt->execute([$user, $pass]);
    $result = $stmt->fetch();
    $stmt->closeCursor();
    if (empty($result['user_id'])) {
	$this->logger->warning($user . " login failed");
        return $response->withStatus(401);
    } else {
        $token = bin2hex(openssl_random_pseudo_bytes(8));
        $stmt = $this->db->prepare("UPDATE user SET token = ?, ts_token_expire = ADDTIME(CURRENT_TIMESTAMP, '01:00:00')");
        $stmt->execute([$token]);
	$response->getBody()->write('{"token":"' . $token . '","userid":' . $result['user_id'] .
				    ',"name":"' . $result['name'] . '","email":"' . $result['email'] .
				    ',"tz":"' . $result['tz'] . '","locale":"' . $result['locale'] .
				    '","image_url":"' . $result['image_url'] . '","admin":' . $result['admin'] .
				    ',"active":' . $result['active'] . '}');
        $this->logger->info($user . " logged in");
    }
    return $response;
});

$app->delete('/websess', function($request, $response, $args) {
    $body = $request->getParsedBody();
    $token = $body['token'];
    $stmt = $this->db->prepare("UPDATE user SET token = NULL, ts_token_expire = NULL WHERE token = ?");
    $numrows = $stmt->execute([$token]);
    if ($numrows > 0) {
        return $response;
    } else {
        return $response->withStatus(401);
    }
});
